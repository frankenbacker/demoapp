package org.bitbucket.frankenbacker.demoapp

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

class DemoApp : Application() {
    companion object {
        lateinit var instance: DemoApp
    }

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var token: String

    override fun onCreate() {
        super.onCreate()
        instance = this

        sharedPreferences = initSharedPreferences()
        token = sharedPreferences.getString("access_token", "")
    }

    private fun initSharedPreferences() =
            getSharedPreferences(getString(R.string.pref_key), Context.MODE_PRIVATE)

    fun getToken() = token

    fun setToken(token: String) {
        this.token = token
        sharedPreferences.edit().putString("access_token", token).apply()
    }
}
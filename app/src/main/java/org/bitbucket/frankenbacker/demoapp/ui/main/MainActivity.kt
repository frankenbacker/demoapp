package org.bitbucket.frankenbacker.demoapp.ui.main

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import org.bitbucket.frankenbacker.demoapp.R

class MainActivity : MvpAppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
package org.bitbucket.frankenbacker.demoapp.ui.auth

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import org.bitbucket.frankenbacker.demoapp.R

class AuthActivity : MvpAppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }
}
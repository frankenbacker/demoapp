package org.bitbucket.frankenbacker.demoapp.ui.launch

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import org.bitbucket.frankenbacker.demoapp.DemoApp
import org.bitbucket.frankenbacker.demoapp.R
import org.bitbucket.frankenbacker.demoapp.presentation.launch.LaunchView
import org.bitbucket.frankenbacker.demoapp.ui.auth.AuthActivity
import org.bitbucket.frankenbacker.demoapp.ui.main.MainActivity

class LaunchActivity : MvpAppCompatActivity(), LaunchView {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)

        val app = DemoApp.instance
        println("token:" + app.getToken())
        app.setToken("123123123")
        println("token:" + app.getToken())
    }

    override fun initMainScreen(isValidToken: Boolean) {
        if (isValidToken) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            startActivity(Intent(this, AuthActivity::class.java))
            finish()
        }
    }
}
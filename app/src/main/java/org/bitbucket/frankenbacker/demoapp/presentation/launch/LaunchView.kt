package org.bitbucket.frankenbacker.demoapp.presentation.launch

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface LaunchView : MvpView {
    fun initMainScreen(isValidToken: Boolean)
}
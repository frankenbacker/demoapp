package org.bitbucket.frankenbacker.demoapp.presentation.launch

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class LaunchPresenter : MvpPresenter<LaunchView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        // Валидация токена
    }
}